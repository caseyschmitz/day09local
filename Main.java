public class Main
{
    public static void main(String[] args)
    {
    
    Food[] allTheFood = new Food(5);
    
    allTheFood[0] = new Wings();
    allTheFood[0].setDescription("Wings, but not spicy wings.");
    Wings wingsReference = (Wings)allTheFood[0];                        //casting wings as a food item to obtain Wings characteristics
    allTheFood[0].wingReference.setRating(20000);
    
    allTheFood[1] = new Food();
    allTheFood[1].setDescription("Chinese");
    
    allTheFood[2] = new Food();
    allTheFood[2].setDescription("Sushi");
    
    allTheFood[3] = new Food();
    allTheFood[3].setDescription("Popcorn");
    
    System.out.println(allTheFood[0].toString());
    
        System.out.println("Let's have a party. People better bring a lot of food.");
    }
}